package cz.gopas.kalkulacka.history

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class HistoryFragment : Fragment(R.layout.fragment_history) {
    private val viewModel: HistoryViewModel by viewModels({ requireActivity() })

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val recycler = view as RecyclerView
        recycler.setHasFixedSize(true)
        val adapter = HistoryAdapter {
            Log.d(TAG, "Clicked $it")
            viewModel.selected.value = it
            findNavController().popBackStack()
        }
        recycler.adapter = adapter
        viewModel.db.getAll().onEach {
            adapter.submitList(it)
        }.launchIn(viewLifecycleOwner.lifecycleScope)
        super.onViewCreated(view, savedInstanceState)
    }

    private companion object {
        private val TAG = HistoryFragment::class.simpleName
    }
}