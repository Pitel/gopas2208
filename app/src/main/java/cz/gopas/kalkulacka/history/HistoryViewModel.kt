package cz.gopas.kalkulacka.history

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.room.Room
import kotlinx.coroutines.flow.MutableStateFlow

class HistoryViewModel(app: Application) : AndroidViewModel(app) {
    init {
        Log.d(TAG, "$this")
    }

    val db = Room.databaseBuilder(app, HistoryDatabase::class.java, "history")
//        .allowMainThreadQueries()
        .build()
        .history()

    val selected = MutableStateFlow<Float?>(null)

    private companion object {
        private val TAG = HistoryViewModel::class.simpleName
    }
}