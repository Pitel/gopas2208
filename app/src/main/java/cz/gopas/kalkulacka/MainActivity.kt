package cz.gopas.kalkulacka

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import cz.gopas.kalkulacka.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
//    private val aText: TextInputEditText by lazy { findViewById(R.id.a_text) }
//    private val bText: TextInputEditText by lazy { findViewById(R.id.b_text) }
//    private val ops: RadioGroup by lazy { findViewById(R.id.ops) }
//    private val result: MaterialTextView by lazy { findViewById(R.id.result) }

    private lateinit var binding: ActivityMainBinding

    init {
//        FragmentManager.enableDebugLogging(BuildConfig.DEBUG)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onCreate")

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val navController = binding.container.getFragment<NavHostFragment>().navController
        setupActionBarWithNavController(navController, AppBarConfiguration(navController.graph))

        Log.d(TAG, "${intent.dataString}")
    }

    override fun onResume() {
        super.onResume()
        Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onResume")

        Snackbar.make(binding.root, "onResume", Snackbar.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show()
        Log.d(TAG, "onPause")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSupportNavigateUp() =
        findNavController(R.id.container).navigateUp() || super.onSupportNavigateUp()

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.about -> {
            findNavController(R.id.container).navigate(NavGraphDirections.actionGlobalAboutFragment())
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private companion object {
        private val TAG: String? = MainActivity::class.simpleName
    }
}