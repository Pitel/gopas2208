package cz.gopas.kalkulacka.calc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.PickVisualMediaRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import cz.gopas.kalkulacka.R
import cz.gopas.kalkulacka.databinding.FragmentCalcBinding
import cz.gopas.kalkulacka.history.HistoryEntity
import cz.gopas.kalkulacka.history.HistoryViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withContext

class CalcFragment : Fragment() {
    private var binding: FragmentCalcBinding? = null
    private val viewModel: CalcViewModel by viewModels()
    private val historyViewModel: HistoryViewModel by viewModels({ requireActivity() })

    private val picker =
        registerForActivityResult(ActivityResultContracts.PickVisualMedia()) { uri ->
            Log.d(TAG, "$uri")
            binding?.image?.setImageURI(uri)
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d(TAG, "onCreateView $this")
        binding = FragmentCalcBinding.inflate(inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.run {
            calc.setOnClickListener {
                val a = aText.text.toString().toFloatOrNull() ?: Float.NaN
                val b = bText.text.toString().toFloatOrNull() ?: Float.NaN
                val op = ops.checkedRadioButtonId
                if (op == R.id.div && b == 0f) {
                    findNavController().navigate(CalcFragmentDirections.actionCalcFragmentToZeroDialog())
                } else {
                    viewModel.calc(a, b, op)
                }
            }
            share.setOnClickListener { share() }
            image.setOnClickListener {
                picker.launch(
                    PickVisualMediaRequest(
                        ActivityResultContracts.PickVisualMedia.ImageOnly
                    )
                )
            }
            ans.setOnClickListener {
                bText.setText(viewModel.ans.toString())
            }
            history.setOnClickListener {
                findNavController().navigate(CalcFragmentDirections.actionCalcFragmentToHistoryFragment())
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putCharSequence(RESULT_KEY, binding?.result?.text)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewStateRestored")
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.getCharSequence(RESULT_KEY)?.let {
            binding?.result?.text = it
        }
    }

    override fun onStart() {
        super.onStart()
        binding?.run {
            viewModel.result.onEach { res ->
                if (res != null) {
                    result.text = "$res"
                    withContext(Dispatchers.IO) {
                        historyViewModel.db.insert(HistoryEntity(res))
                    }
                    viewModel.result.value = null
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)

            historyViewModel.selected.onEach {
                Log.d(TAG, "Clicked $it")
                if (it != null) {
                    Log.d(TAG, "setText")
                    aText.setText("$it")
                    historyViewModel.selected.value = null
                }
            }.launchIn(viewLifecycleOwner.lifecycleScope)
        }
    }

    override fun onDestroyView() {
        binding = null
        Log.d(TAG, "onDestroyView $this")
        super.onDestroyView()
    }

    private fun share() {
        Log.d(TAG, "Share!")
        val intent = Intent(Intent.ACTION_SEND)
            .putExtra(Intent.EXTRA_TEXT, getString(R.string.share_text, binding?.result?.text))
            .setType("text/plain")
        startActivity(intent)
    }

    private companion object {
        private val TAG = CalcFragment::class.simpleName
        private const val RESULT_KEY = "result"
    }
}