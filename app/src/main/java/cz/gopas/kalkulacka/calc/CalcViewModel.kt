package cz.gopas.kalkulacka.calc

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.annotation.IdRes
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import cz.gopas.kalkulacka.R
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlin.time.Duration.Companion.seconds

class CalcViewModel(app: Application) : AndroidViewModel(app) {
    private val prefs = app.getSharedPreferences("my name", Context.MODE_PRIVATE)

    val result = MutableStateFlow<Float?>(null)

    val ans: Float
        get() {
            val ans = prefs.getFloat(ANS_KEY, Float.NaN)
            Log.d(TAG, "get $ans")
            return ans
        }

    fun calc(a: Float, b: Float, @IdRes op: Int) {
        Log.d(TAG, "Calc!")
        viewModelScope.launch {
            val result = when (op) {
                R.id.add -> a + b
                R.id.sub -> a - b
                R.id.mul -> a * b
                R.id.div -> a / b
                else -> Float.NaN
            }
            prefs.edit {
                putFloat(ANS_KEY, result)
            }
            delay(2.seconds)
            this@CalcViewModel.result.value = result
        }
    }

    private companion object {
        private val TAG = CalcViewModel::class.simpleName
        private const val ANS_KEY = "ans"
    }
}